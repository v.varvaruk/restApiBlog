package com.v.varvaruk.restApiBlog.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@ApiIgnore
public class HelloController {

    @RequestMapping("/")
    public String home() {
        return "redirect:/swagger-ui.html";
    }

}
