package com.v.varvaruk.restApiBlog.controllers;

import com.v.varvaruk.restApiBlog.dto.post.PostDTO;
import com.v.varvaruk.restApiBlog.dto.post.PostDTOCreate;
import com.v.varvaruk.restApiBlog.dto.post.PostDTOUpdate;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.exception.AccessException;
import com.v.varvaruk.restApiBlog.exception.ThereIsNoSuchPostException;
import com.v.varvaruk.restApiBlog.mappers.post.PostMapper;
import com.v.varvaruk.restApiBlog.mappers.post.PostMapperCreate;
import com.v.varvaruk.restApiBlog.mappers.post.PostMapperUpdate;
import com.v.varvaruk.restApiBlog.payload.ApiResponse;
import com.v.varvaruk.restApiBlog.security.CurrentUser;
import com.v.varvaruk.restApiBlog.security.UserPrincipal;
import com.v.varvaruk.restApiBlog.services.mark.MarkService;
import com.v.varvaruk.restApiBlog.services.post.PostService;
import com.v.varvaruk.restApiBlog.services.user.MyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/post")
@Api(description = "Set of endpoints for Creating, Retrieving, Updating and Deleting of Posts.")
public class PostController {

    @Autowired
    PostMapper postMapper;
    @Autowired
    PostMapperCreate postMapperCreate;
    @Autowired
    PostMapperUpdate postMapperUpdate;
    @Autowired
    PostService postService;

    @Autowired
    MyUserService myUserService;

    @Autowired
    MarkService markService;

    @ApiOperation(value = "Creating a new post.", response = PostDTO.class, authorizations = {@Authorization(value = "Bearer")})
    @PostMapping(value = "/")
    public ResponseEntity<PostDTO> createPost(@ApiIgnore @CurrentUser UserPrincipal userPrincipal, @RequestBody PostDTOCreate postDTOCreate) {
        Post post = postMapperCreate.dtoToEntity(postDTOCreate);
        return new ResponseEntity<>(postMapper.entityToDto(postService.createPost(post, userPrincipal)), HttpStatus.OK);
    }

    @ApiOperation(value = "Get post by id.", response = PostDTO.class, authorizations = {@Authorization(value = "Bearer")})
    @GetMapping(value = "/{id}")
    public ResponseEntity<PostDTO> getPost(@PathVariable("id") Long id) {
        Post post = postService.getPostById(id).orElseThrow(ThereIsNoSuchPostException::new);
        return new ResponseEntity<>(postMapper.entityToDto(post), HttpStatus.OK);
    }


    @ApiOperation(value = "Get posts by Mark.", response = PostDTO.class, responseContainer = "Set", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping(value = "/mark/{mark}")
    public ResponseEntity<?> getPostsByMark(@PathVariable("mark") String mark) {
        Set<PostDTO> postDTOS = postMapper.postsToPostDtos(postService.findPostsByMark(mark));
        return new ResponseEntity<>(postDTOS, HttpStatus.OK);
    }

    @ApiOperation(value = "Get posts by date created.", response = PostDTO.class, responseContainer = "Set", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping(value = "/date/created/{dateCreated}")
    public ResponseEntity<?> findPostsByDateCreatedEquals(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date dateCreated) {
        Set<Post> posts = postService.findPostsByDateCreatedEquals(dateCreated);
        Set<PostDTO> postDTOS = postMapper.postsToPostDtos(posts);
        return new ResponseEntity<>(postDTOS, HttpStatus.OK);
    }

    @ApiOperation(value = "Get posts by date of editing.", response = PostDTO.class, responseContainer = "Set", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping(value = "/date/editing/{dateOfEditing}")
    public ResponseEntity<?> findPostsByDateOfEditingEquals(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date dateOfEditing) {
        Set<Post> posts = postService.findPostsByDateOfEditingEquals(dateOfEditing);
        Set<PostDTO> postDTOS = postMapper.postsToPostDtos(posts);
        return new ResponseEntity<>(postDTOS, HttpStatus.OK);
    }

    @ApiOperation(value = "Get posts by UserAuthor id.", response = PostDTO.class, responseContainer = "Set", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping(value = "/author/{id}")
    public ResponseEntity<?> getPostByUserAuthor(@PathVariable("id") Long id) {
        Set<Post> posts = postService.findPostsByUserAuthorEquals(id);
        Set<PostDTO> postDTOS = postMapper.postsToPostDtos(posts);
        return new ResponseEntity<>(postDTOS, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete post by id.", response = ApiResponse.class, authorizations = {@Authorization(value = "Bearer")})
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deletePost(@ApiIgnore @CurrentUser UserPrincipal userPrincipal, @PathVariable("id") Long id) {
        postService.delete(userPrincipal, id);
        return new ResponseEntity(new ApiResponse(true, "Post " + id.toString() + " removed!"),
                HttpStatus.OK);


    }

    @ApiOperation(value = "Get all posts.", response = PostDTO.class, responseContainer = "Set", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping(value = "/all")
    public ResponseEntity<?> getPostsAll() {
        Iterable<Post> posts = postService.postsAll();
        Set<PostDTO> postDTOs = new HashSet<PostDTO>();
        for (Post post : posts) {
            postDTOs.add(postMapper.entityToDto(post));
        }
        return new ResponseEntity<>(postDTOs, HttpStatus.OK);
    }

    @ApiOperation(value = "Get all posts in page. PageNumber defaultValue=1.PageSize defaultValue=1."
            , response = PostDTO.class, responseContainer = "Set", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping(value = "/all/page")
    public ResponseEntity<?> getPostsAllPage(
            @RequestParam(name = "p", defaultValue = "1") int pageNumber,
            @RequestParam(name = "s", defaultValue = "1") int pageSize) {

        List<Post> postList = postService.findPostsAllPage(pageNumber, pageSize);
        Set<PostDTO> postDTOs = new HashSet<PostDTO>();
        for (Post post : postList) {
            postDTOs.add(postMapper.entityToDto(post));
        }
        return new ResponseEntity<>(postDTOs, HttpStatus.OK);
    }

    @ApiOperation(value = "Update post.Possible only for the author or administrator.", response = PostDTO.class, authorizations = {@Authorization(value = "Bearer")})
    @PostMapping(value = "/update")
    public ResponseEntity<?> updatePost(@ApiIgnore @CurrentUser UserPrincipal userPrincipal, @RequestBody PostDTOUpdate postDTOUpdate) {
        Post post = postMapperUpdate.dtoToEntity(postDTOUpdate);
        Post postUpdate = postService.updatePost(post, userPrincipal);
        if (postUpdate.equals(post)) {
            throw new AccessException();
        }
        PostDTO postDTOResp = postMapper.entityToDto(postUpdate);
        return new ResponseEntity<>(postDTOResp, HttpStatus.OK);
    }

}
