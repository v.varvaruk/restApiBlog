package com.v.varvaruk.restApiBlog.controllers;

import com.v.varvaruk.restApiBlog.entities.Role;
import com.v.varvaruk.restApiBlog.entities.RoleName;
import com.v.varvaruk.restApiBlog.entities.User;
import com.v.varvaruk.restApiBlog.exception.AppException;
import com.v.varvaruk.restApiBlog.payload.ApiResponse;
import com.v.varvaruk.restApiBlog.payload.JwtAuthenticationResponse;
import com.v.varvaruk.restApiBlog.payload.LoginRequest;
import com.v.varvaruk.restApiBlog.payload.SignUpRequest;
import com.v.varvaruk.restApiBlog.security.JwtTokenProvider;
import com.v.varvaruk.restApiBlog.services.role.MyRoleService;
import com.v.varvaruk.restApiBlog.services.user.MyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

@RestController
@RequestMapping("/api/auth")
@Api(description = "Set of endpoints for register and authenticate new users.")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    MyUserService myUserService;

    @Autowired
    MyRoleService myRoleService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @PostMapping("/signin")
    @ApiOperation(value = "Authenticate user.",response = JwtAuthenticationResponse.class)
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    @ApiOperation(value = "Register new user.",response = ApiResponse.class)
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
         if((myUserService.existsByUsername(signUpRequest.getUsername()))
                 |((myUserService.existsByName(signUpRequest.getName()))) ){

            return new ResponseEntity(new ApiResponse(false, "Username or name is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if (myUserService.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }


        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                        signUpRequest.getEmail(), signUpRequest.getPassword());


        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Role userRole = myRoleService.findByName(RoleName.ROLE_USER)
                .orElseThrow(() -> new AppException("User Role not set."));

        user.setRoles(Collections.singleton(userRole));

        User result = myUserService.save(user);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }
}