package com.v.varvaruk.restApiBlog.controllers;

import com.v.varvaruk.restApiBlog.dto.post.PostDTO;
import com.v.varvaruk.restApiBlog.dto.comment.CommentDTO;
import com.v.varvaruk.restApiBlog.dto.comment.CommentDTOCreate;
import com.v.varvaruk.restApiBlog.dto.comment.CommentDTOUpdate;
import com.v.varvaruk.restApiBlog.entities.Comment;
import com.v.varvaruk.restApiBlog.exception.AccessException;
import com.v.varvaruk.restApiBlog.mappers.comment.CommentMapper;
import com.v.varvaruk.restApiBlog.mappers.comment.CommentMapperCreate;
import com.v.varvaruk.restApiBlog.mappers.comment.CommentMapperUpdate;
import com.v.varvaruk.restApiBlog.payload.ApiResponse;
import com.v.varvaruk.restApiBlog.security.CurrentUser;
import com.v.varvaruk.restApiBlog.security.UserPrincipal;
import com.v.varvaruk.restApiBlog.services.comment.CommentService;
import com.v.varvaruk.restApiBlog.services.user.MyUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;
import java.util.Set;

@RestController
@RequestMapping("/api/comment")
@Api(description = "Set of endpoints for Creating, Retrieving, Updating and Deleting of Comments.")
public class CommentController {

    @Autowired
    CommentMapper commentMapper;

    @Autowired
    CommentMapperCreate commentMapperCreate;
    @Autowired
    CommentMapperUpdate commentMapperUpdate;

    @Autowired
    CommentService commentService;

    @Autowired
    MyUserService myUserService;

    @ApiOperation(value = "Creating a new comment by post ID.", response = CommentDTO.class,authorizations = { @Authorization(value="Bearer") })
    @PostMapping(value = "/post/{id}")
    public ResponseEntity<CommentDTO> createComment(@ApiIgnore @CurrentUser UserPrincipal userAuthor
            , @RequestBody CommentDTOCreate commentDTO
            , @PathVariable("id") Long id) {
        Comment comment = commentMapperCreate.dtoToEntity(commentDTO);
        Comment commentCreated = commentService.createComment(comment, userAuthor, id);
        return new ResponseEntity<>(commentMapper.entityToDto(commentCreated), HttpStatus.OK);
    }


    @ApiOperation(value = "Get comments  by  post id.", response = PostDTO.class, responseContainer = "Set",authorizations = { @Authorization(value="Bearer") })
    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getCommentsByPostId(@PathVariable("id") Long id) {
               return new ResponseEntity<>(commentMapper.commentsToCommentDTOS(commentService.findByPostEquals(id)),
                HttpStatus.OK);
    }


    @ApiOperation(value = "Get comments by date created."
            , response = CommentDTO.class, responseContainer = "Set",authorizations = { @Authorization(value="Bearer") })
    @GetMapping(value = "/date/create/{dateCreated}")
    public ResponseEntity<?> findByDateCreatedEquals(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date dateCreated) {
        Set<Comment> comments = commentService.findByDateCreatedEquals(dateCreated);
        Set<CommentDTO> commentDTOS = commentMapper.commentsToCommentDTOS(comments);
        return new ResponseEntity<>(commentDTOS, HttpStatus.OK);
    }

    @ApiOperation(value = "Get comments by date of editing.", response = CommentDTO.class, responseContainer = "Set",authorizations = { @Authorization(value="Bearer") })
    @GetMapping(value = "/date/editing/{dateOfEditing}")
    public ResponseEntity<?> findCommentsByDateOfEditingEquals(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date dateOfEditing) {
        Set<Comment> comments = commentService.findByDateOfEditingEquals(dateOfEditing);
        Set<CommentDTO> commentDTOS = commentMapper.commentsToCommentDTOS(comments);
        return new ResponseEntity<>(commentDTOS, HttpStatus.OK);
    }

    @ApiOperation(value = "Get comments by UserAuthor id.", response = CommentDTO.class,authorizations = { @Authorization(value="Bearer") })
    @GetMapping(value = "/author/{id}")
    public ResponseEntity<?> getCommentByUserAuthor(@PathVariable("id") Long id) {
        Set<Comment> comments = commentService.findByUserAuthorEquals(id);
        Set<CommentDTO> commentDTO = commentMapper.commentsToCommentDTOS(comments);
        return new ResponseEntity<>(commentDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "Update comment.Possible only for the author or administrator.", response = CommentDTO.class,authorizations = { @Authorization(value="Bearer") })
    @PostMapping(value = "/update")
    public ResponseEntity<?> updateComment(@ApiIgnore @CurrentUser UserPrincipal userPrincipal, @RequestBody CommentDTOUpdate commentDTOUpdate) {
        Comment comment = commentMapperUpdate.dtoToEntity(commentDTOUpdate);
        Comment commentUpdate = commentService.updateComment(comment, userPrincipal);
        CommentDTO commentDTOResp = commentMapper.entityToDto(commentUpdate);
        if (commentUpdate.equals(comment)) {
            throw new AccessException();
        }
        return new ResponseEntity<>(commentDTOResp, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete comment by id.", response = ApiResponse.class,authorizations = { @Authorization(value="Bearer") })
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteComment(@PathVariable("id") Long id) {
        commentService.delete(id);
        return new ResponseEntity(new ApiResponse(true, "Post " + id.toString() + " removed!"),
                HttpStatus.OK);

    }


}
