package com.v.varvaruk.restApiBlog.services.mark;

import com.v.varvaruk.restApiBlog.entities.Mark;

import java.util.List;
import java.util.Optional;

public interface MarkService {

    Mark create(Mark mark);

    Optional<Mark> getMarkById(Long id);

    boolean delete(Long id);


    List<Mark> marksAll();

    Optional<Mark> findByNameEquals(String name);



    Mark updateMark(Mark mark);


}
