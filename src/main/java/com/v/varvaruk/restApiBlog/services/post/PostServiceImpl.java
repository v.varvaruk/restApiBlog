package com.v.varvaruk.restApiBlog.services.post;

import com.v.varvaruk.restApiBlog.entities.Mark;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.entities.Role;
import com.v.varvaruk.restApiBlog.entities.User;
import com.v.varvaruk.restApiBlog.exception.AccessException;
import com.v.varvaruk.restApiBlog.exception.ThereIsNoSuchAuthorException;
import com.v.varvaruk.restApiBlog.exception.ThereIsNoSuchMarkException;
import com.v.varvaruk.restApiBlog.exception.ThereIsNoSuchPostException;
import com.v.varvaruk.restApiBlog.repo.PostRepository;
import com.v.varvaruk.restApiBlog.security.UserPrincipal;
import com.v.varvaruk.restApiBlog.services.mark.MarkService;
import com.v.varvaruk.restApiBlog.services.user.MyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

import static com.v.varvaruk.restApiBlog.entities.RoleName.ROLE_ADMIN;


@Service
public class PostServiceImpl implements PostService {

    @Autowired
    PostRepository postRepository;

    @Autowired
    MyUserService myUserService;

    @Autowired
    MarkService markService;

    @Transactional
    @Override
    public Post createPost(Post post, UserPrincipal userPrincipal) {
        User userAuthor = myUserService.findByUserId(userPrincipal.getId()).orElseThrow(ThereIsNoSuchAuthorException::new);
        post.setDateCreated(new Date());
        post.setDateOfEditing(new Date());
        post = checkMarkInNewPost(post);
        post.setUserAuthor(userAuthor);
        return postRepository.save(post);
    }

    @Transactional
    @Override
    public boolean delete(UserPrincipal userPrincipal, Long id) throws IllegalArgumentException {
        User userAuthor = myUserService.findByUserId(userPrincipal.getId()).orElseThrow(ThereIsNoSuchAuthorException::new);
        Post postInDB = getPostById(id).orElseThrow(ThereIsNoSuchPostException::new);
        if (!((postInDB.getUserAuthor().equals(userAuthor)) | (userAuthor.getRoles()).contains(Role.builder().name(ROLE_ADMIN).build()))) {
            throw new AccessException();
        }

        verificationOfABundleWithMark(postInDB);
        postRepository.save(postInDB);
        postRepository.deleteById(id);
        return true;

    }

    @Override
    public Optional<Post> getPostById(Long id) {

        return postRepository.findById(id);

    }

    @Transactional
    @Override
    public Post updatePost(Post post, UserPrincipal userPrincipal) {
        User userAuthor = myUserService.findByUserId(userPrincipal.getId()).orElseThrow(ThereIsNoSuchAuthorException::new);
        Post postInDB = getPostById(post.getId()).orElseThrow(ThereIsNoSuchPostException::new);
        if (postInDB.getUserAuthor().equals(userAuthor) | (userAuthor.getRoles()).contains(Role.builder().name(ROLE_ADMIN).build())) {
            String title = ((post.getTitle().equals(postInDB.getTitle())) & (!(post.getTitle().isEmpty()))) ? postInDB.getTitle() : post.getTitle();
            String body = ((post.getBody().equals(postInDB.getBody())) & (!(post.getBody().isEmpty()))) ? postInDB.getBody() : post.getBody();
            postInDB.setMarkList(checkMarkInNewPost(post).getMarkList());
            postInDB.setTitle(title);
            postInDB.setBody(body);
            postInDB.setDateOfEditing(new Date());
            postRepository.save(postInDB);
            return postInDB;
        }
        return post;
    }

    @Override
    public Iterable<Post> postsAll() {
        return postRepository.findAll();
    }

    @Override
    public List<Post> findPostsAllPage(int pageNumber, int pageSize) {
        PageRequest request = PageRequest.of(pageNumber - 1, pageSize, Sort.Direction.ASC, "id");
        return postRepository.findAll(request).getContent();
    }

    @Override
    public Set<Post> findPostsByUserAuthorEquals(Long userAuthorID) {
        User userAuthor = myUserService.findByUserId(userAuthorID).orElseThrow(ThereIsNoSuchAuthorException::new);
        return postRepository.findPostsByUserAuthorEquals(userAuthor);
    }

    @Override
    public Set<Post> findPostsByMarkListEquals(Set<Mark> markList) {
        return postRepository.findPostsByMarkListEquals(markList);
    }

    @Override
    public Set<Post> findPostsByDateCreatedEquals(Date dateCreated) {
        return postRepository.findPostsByDateCreatedEquals(dateCreated);
    }

    @Override
    public Set<Post> findPostsByDateOfEditingEquals(Date dateOfEditing) {
        return postRepository.findPostsByDateOfEditingEquals(dateOfEditing);
    }

    @Override
    public Set<Post> findPostsByMark(String mark) {
        Mark markInDb = markService.findByNameEquals(mark).orElseThrow
                (ThereIsNoSuchMarkException::new);
        return findPostsByMarkListEquals(Collections.singleton(markInDb));
    }

    @Override
    public Post checkMarkInNewPost(Post post) {

        try {

            if (!(post.getMarkList().contains(null))) {
                Set<Mark> marks = new HashSet<>();

                for (Mark mark : post.getMarkList()
                        ) {
                    marks.add(markService.updateMark(mark));
                }

                post.setMarkList(marks);
                return post;

            }
        } catch (NullPointerException e) {
            System.out.print("Mark List null " + e);
        }


        return post;
    }

    @Override
    public Post verificationOfABundleWithMark(Post myPost) {

        Set<Mark> markList = myPost.getMarkList();
        Set<Mark> result = new HashSet<>();

        for (Mark mark : markList) {
            if (!(findPostsByMark(mark.getName()).size() > 1)) {
                result.add(mark);
            }

        }

        myPost.setMarkList(result);
        return myPost;
    }


}
