package com.v.varvaruk.restApiBlog.services.comment;

import com.v.varvaruk.restApiBlog.entities.Comment;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.entities.Role;
import com.v.varvaruk.restApiBlog.entities.User;
import com.v.varvaruk.restApiBlog.exception.ThereIsNoSuchAuthorException;
import com.v.varvaruk.restApiBlog.exception.ThereIsNoSuchCommentException;
import com.v.varvaruk.restApiBlog.exception.ThereIsNoSuchPostException;
import com.v.varvaruk.restApiBlog.repo.CommentRepository;
import com.v.varvaruk.restApiBlog.security.UserPrincipal;
import com.v.varvaruk.restApiBlog.services.post.PostService;
import com.v.varvaruk.restApiBlog.services.user.MyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.v.varvaruk.restApiBlog.entities.RoleName.ROLE_ADMIN;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentRepository commentRepository;

    @Autowired
    PostService postService;

    @Autowired
    MyUserService myUserService;

    @Transactional
    @Override
    public Comment createComment(Comment comment, UserPrincipal userAuthor, Long postId) {
        User user = myUserService.findByUserId(userAuthor.getId()).orElseThrow(ThereIsNoSuchPostException::new);
        comment.setUserAuthor(user);
        comment.setDateCreated(new Date());
        comment.setDateOfEditing(new Date());
        Post post = postService.getPostById(postId).orElseThrow(ThereIsNoSuchPostException::new);
        comment.setPost(post);
        return commentRepository.save(comment);
    }

    @Override
    public Optional<Comment> getCommentById(Long id) {
        return commentRepository.findById(id);
    }

    @Transactional
    @Override
    public Comment updateComment(Comment comment, UserPrincipal userPrincipal) {

        Comment commentInDB = getCommentById(comment.getId()).orElseThrow(ThereIsNoSuchCommentException::new);
        User userAuthor = myUserService.findByUserId(userPrincipal.getId()).orElseThrow(ThereIsNoSuchPostException::new);
        if (commentInDB.getUserAuthor().equals(userAuthor) | (userAuthor.getRoles()).contains(Role.builder().name(ROLE_ADMIN).build())) {
            String text = ((comment.getText().equals(commentInDB.getText())) & (!(comment.getText().isEmpty()))) ? commentInDB.getText() : comment.getText();
            commentInDB.setText(text);
            commentInDB.setDateOfEditing(new Date());
            commentRepository.save(commentInDB);
            return commentInDB;
        }

        return comment;
    }

    @Transactional
    @Override
    public boolean delete(Long id) throws IllegalArgumentException {
        commentRepository.deleteById(id);
        return true;
    }

    @Override
    public List<Comment> commentsAll() {
        return commentRepository.findAll();
    }

    @Override
    public Set<Comment> findByDateCreatedEquals(Date dateCreated) {
        return commentRepository.findByDateCreatedEquals(dateCreated);
    }

    @Override
    public Set<Comment> findByDateOfEditingEquals(Date dateOfEditing) {
        return commentRepository.findByDateOfEditingEquals(dateOfEditing);
    }

    @Override
    public Set<Comment> findByUserAuthorEquals(Long id) {
      User userAuthor =  myUserService.findByUserId(id).orElseThrow(ThereIsNoSuchAuthorException::new);


        return commentRepository.findByUserAuthorEquals(userAuthor);
    }

    @Override
    public Set<Comment> findByPostEquals(Long  postId) {
        Post post = postService.getPostById(postId).orElseThrow(ThereIsNoSuchPostException::new);
        return commentRepository.findByPostEquals(post);
    }
}
