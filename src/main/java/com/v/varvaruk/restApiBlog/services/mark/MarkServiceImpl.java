package com.v.varvaruk.restApiBlog.services.mark;

import com.v.varvaruk.restApiBlog.entities.Mark;
import com.v.varvaruk.restApiBlog.repo.MarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class MarkServiceImpl implements MarkService {
    @Autowired
    MarkRepository markRepository;

    @Transactional
    @Override
    public Mark create(Mark mark) {
        return markRepository.save(mark);
    }

    @Override
    public Optional<Mark> getMarkById(Long id) {
        return markRepository.findById(id);
    }

    @Transactional
    @Override
    public boolean delete(Long id) throws IllegalArgumentException {
        markRepository.deleteById(id);
        return true;
    }

    @Override
    public List<Mark> marksAll() {
        return markRepository.findAll();
    }

    @Override
    public Optional<Mark> findByNameEquals(String name) {
        return markRepository.findByNameEquals(name);
    }

    @Transactional
    @Override
    public Mark updateMark(Mark mark) {
        Mark markDb = mark;
        if (findByNameEquals(mark.getName()).isPresent()) {

            markDb = findByNameEquals(mark.getName()).get();
        } else {
            markDb = create(mark);
        }
        return markDb;
    }


}
