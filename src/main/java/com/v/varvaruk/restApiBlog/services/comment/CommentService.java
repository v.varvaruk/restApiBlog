package com.v.varvaruk.restApiBlog.services.comment;

import com.v.varvaruk.restApiBlog.entities.Comment;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.security.UserPrincipal;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface CommentService {
    Comment createComment(Comment comment, UserPrincipal userAuthor, Long postId);
    Optional<Comment>getCommentById(Long id);
    Comment  updateComment (Comment comment, UserPrincipal userAuthor);
    boolean delete (Long id);
    List<Comment> commentsAll();
    Set<Comment> findByDateCreatedEquals(Date dateCreated );
    Set<Comment> findByDateOfEditingEquals(Date dateOfEditing);
    Set<Comment> findByUserAuthorEquals(Long id);
    Set<Comment> findByPostEquals(Long postId);


}
