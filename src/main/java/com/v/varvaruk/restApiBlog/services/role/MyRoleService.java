package com.v.varvaruk.restApiBlog.services.role;

import com.v.varvaruk.restApiBlog.entities.Role;
import com.v.varvaruk.restApiBlog.entities.RoleName;
import org.springframework.stereotype.Service;

import java.util.Optional;

public interface MyRoleService {

    Optional<Role> findByName(RoleName roleName);
}
