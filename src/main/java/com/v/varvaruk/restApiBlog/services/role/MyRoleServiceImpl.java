package com.v.varvaruk.restApiBlog.services.role;

import com.v.varvaruk.restApiBlog.entities.Role;
import com.v.varvaruk.restApiBlog.entities.RoleName;
import com.v.varvaruk.restApiBlog.repo.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class MyRoleServiceImpl implements MyRoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public Optional<Role> findByName(RoleName roleName) {
        return roleRepository.findByName(roleName);
    }
}
