package com.v.varvaruk.restApiBlog.services.post;

import com.v.varvaruk.restApiBlog.entities.Mark;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.security.UserPrincipal;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PostService {
    Post createPost(Post post, UserPrincipal userPrincipal);

    boolean delete(UserPrincipal userPrincipal,Long id);

    Optional<Post> getPostById(Long id);

    Post updatePost(Post post, UserPrincipal userPrincipal);

    Iterable<Post> postsAll();

    List<Post> findPostsAllPage(int pageNumber, int pageSize);

    Set<Post> findPostsByUserAuthorEquals(Long userAuthorID);

    Set<Post> findPostsByMarkListEquals(Set<Mark> markList);

    Set<Post> findPostsByDateCreatedEquals(Date dateCreated);

    Set<Post> findPostsByDateOfEditingEquals(Date dateOfEditing);

    Set<Post> findPostsByMark(String mark);

    Post checkMarkInNewPost(Post post);

    Post verificationOfABundleWithMark (Post chekPost);

}
