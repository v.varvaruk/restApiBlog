package com.v.varvaruk.restApiBlog.services.user;

import com.v.varvaruk.restApiBlog.entities.User;

import java.util.List;
import java.util.Optional;

public interface MyUserService {
    Optional<User> findByEmail(String email);

    Optional<User> findByUsernameOrEmail(String username, String email);

    List<User> findByIdIn(List<Long> userIds);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Optional<User> findByUserId(Long id);

    Boolean existsByEmail(String email);
    Boolean existsByName(String name);

    User save(User user) throws IllegalArgumentException;

}


