package com.v.varvaruk.restApiBlog.mappers.post;

import com.v.varvaruk.restApiBlog.dto.post.PostDTOUpdate;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.mappers.role.RoleMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface PostMapperUpdate {

    PostDTOUpdate entityToDto(Post post);

    @InheritInverseConfiguration
    Post dtoToEntity(PostDTOUpdate postDTOUpdate);


    Set<PostDTOUpdate> postsToPostDtos(Set<Post> posts);

    @InheritInverseConfiguration
    Set<Post> postDtosToPosts(Set<PostDTOUpdate> postDTOUpdates);

}
