package com.v.varvaruk.restApiBlog.mappers.comment;

import com.v.varvaruk.restApiBlog.dto.comment.CommentDTOCreate;
import com.v.varvaruk.restApiBlog.entities.Comment;
import com.v.varvaruk.restApiBlog.mappers.role.RoleMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface CommentMapperCreate {
    CommentDTOCreate entityToDto(Comment comment);

    @InheritInverseConfiguration
    Comment dtoToEntity(CommentDTOCreate commentDTO);


    Set<CommentDTOCreate> commentsToCommentDTOS(Set<Comment> comments);

    @InheritInverseConfiguration
    Set<Comment>commentDtosToComments(Set<CommentDTOCreate> commentDTOS);




}
