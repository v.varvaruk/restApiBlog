package com.v.varvaruk.restApiBlog.mappers.role;

import com.v.varvaruk.restApiBlog.dto.role.RoleDTO;
import com.v.varvaruk.restApiBlog.entities.Role;
import com.v.varvaruk.restApiBlog.entities.RoleName;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public class RoleMapper {

    public RoleDTO entityToDto(Role role) {
        RoleDTO roleDTO = RoleDTO.builder().id(role.getId())
                .roleName(role.getName()
                        .name()).build();

        return roleDTO;
    }

    ;



    Role dtoToEntity(RoleDTO roleDTO) {
        Role role = Role.builder()
                .id(roleDTO.getId())
                .name(RoleName.valueOf(roleDTO.getRoleName()))
                .build();
        return role;
    }

    ;
}
