package com.v.varvaruk.restApiBlog.mappers.user;

import com.v.varvaruk.restApiBlog.dto.user.UserDTO;
import com.v.varvaruk.restApiBlog.entities.User;
import com.v.varvaruk.restApiBlog.mappers.role.RoleMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Service;

@Service
@Mapper(componentModel = "spring", uses = {RoleMapper.class},unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {


    UserDTO entityToDto(User user);


    @InheritInverseConfiguration
    User dtoToEntity(UserDTO userDTO);


}
