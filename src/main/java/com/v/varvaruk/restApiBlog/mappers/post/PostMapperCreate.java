package com.v.varvaruk.restApiBlog.mappers.post;

import com.v.varvaruk.restApiBlog.dto.post.PostDTOCreate;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.mappers.role.RoleMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface PostMapperCreate {

    PostDTOCreate entityToDto(Post post);

    @InheritInverseConfiguration
    Post dtoToEntity(PostDTOCreate postDTOCreate);


    Set<PostDTOCreate> postsToPostDtos(Set<Post> posts);

    @InheritInverseConfiguration
    Set<Post> postDtosToPosts(Set<PostDTOCreate> postDTOCreateSett);

}
