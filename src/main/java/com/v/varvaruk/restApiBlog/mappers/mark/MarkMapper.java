package com.v.varvaruk.restApiBlog.mappers.mark;

import com.v.varvaruk.restApiBlog.dto.mark.MarkDTO;
import com.v.varvaruk.restApiBlog.entities.Mark;
import com.v.varvaruk.restApiBlog.mappers.role.RoleMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface MarkMapper {
    MarkDTO entityToDto(Mark mark);

    @InheritInverseConfiguration
    Mark dtoToEntity(MarkDTO markDTO);



    Set<MarkDTO> marksToMarkDtos(Set<Mark> marks);

    @InheritInverseConfiguration
    Set<Mark> markDtosToMarks(Set<MarkDTO> markDTOSet);



}
