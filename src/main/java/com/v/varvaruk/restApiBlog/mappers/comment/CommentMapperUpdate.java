package com.v.varvaruk.restApiBlog.mappers.comment;

import com.v.varvaruk.restApiBlog.dto.comment.CommentDTOUpdate;
import com.v.varvaruk.restApiBlog.entities.Comment;
import com.v.varvaruk.restApiBlog.mappers.role.RoleMapper;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Mapper(componentModel = "spring", uses = {RoleMapper.class})
public interface CommentMapperUpdate {
    CommentDTOUpdate entityToDto(Comment comment);

    @InheritInverseConfiguration
    Comment dtoToEntity(CommentDTOUpdate commentDTO);


    Set<CommentDTOUpdate> commentsToCommentDTOS(Set<Comment> comments);

    @InheritInverseConfiguration
    Set<Comment>commentDtosToComments(Set<CommentDTOUpdate> commentDTOS);




}
