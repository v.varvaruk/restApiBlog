package com.v.varvaruk.restApiBlog.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "You are not allowed access!")
public class AccessException extends RuntimeException  {


}
