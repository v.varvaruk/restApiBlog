package com.v.varvaruk.restApiBlog.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "There is no such mark")
public class ThereIsNoSuchMarkException extends RuntimeException  {


}
