package com.v.varvaruk.restApiBlog.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class AwesomeExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(ThereIsNoSuchPostException.class)
    protected ResponseEntity<AwesomeException> handleThereIsNoSuchPostException() {
        return new ResponseEntity<>(new AwesomeException("There is no such post!"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ThereIsNoSuchCommentException.class)
    protected ResponseEntity<AwesomeException> handleThereIsNoSuchCommentException() {
        return new ResponseEntity<>(new AwesomeException("There is no such comment!"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ThereIsNoSuchMarkException.class)
    protected ResponseEntity<AwesomeException> handleThereIsNoSuchMarkException() {
        return new ResponseEntity<>(new AwesomeException("There is no such mark!"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ThereIsNoSuchAuthorException.class)
    protected ResponseEntity<AwesomeException> handleThereIsNoSuchAuthorException() {
        return new ResponseEntity<>(new AwesomeException("There is no such author!"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    protected ResponseEntity<AwesomeException> handleThereIsNoSuchDeletePostException() {
        return new ResponseEntity<>(new AwesomeException("Entity with id no exists!"), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AccessException.class)
    protected ResponseEntity<AwesomeException> handleThereIsAccessException() {
        return new ResponseEntity<>(new AwesomeException("You are not allowed access!"), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(NullPointerException.class)
    protected ResponseEntity<AwesomeException> handleThereIsNullPointerExceptionException() {
        return new ResponseEntity<>(new AwesomeException("Are you serious? Can not be transferred NULL!"), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<AwesomeException> handleThereIsConstraintViolationException() {
        return new ResponseEntity<>(new AwesomeException("Fill in all the fields!"), HttpStatus.BAD_REQUEST);
    }





    @Data
    @AllArgsConstructor
    private static class AwesomeException {
        private String message;
    }
}
