package com.v.varvaruk.restApiBlog.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "posts")

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    private String title;
    @NotBlank
    private String body;

    private Date dateCreated;

    private Date dateOfEditing;

    @ManyToOne
    @JoinColumn(name = "user_id",nullable = false)
    private User userAuthor;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "posts_marks",
            joinColumns = {@JoinColumn(name = "post_id")},
            inverseJoinColumns = {@JoinColumn(name = "marks_id")})
    private Set<Mark> markList;

    @OneToMany(mappedBy = "post",cascade = CascadeType.REMOVE)
    private List<Comment> commentList;


}
