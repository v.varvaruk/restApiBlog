package com.v.varvaruk.restApiBlog.entities;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}