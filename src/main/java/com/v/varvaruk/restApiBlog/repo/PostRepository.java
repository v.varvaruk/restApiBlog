package com.v.varvaruk.restApiBlog.repo;

import com.v.varvaruk.restApiBlog.entities.Mark;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.entities.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.Set;

public interface PostRepository extends PagingAndSortingRepository<Post,Long> {


    Set<Post> findPostsByUserAuthorEquals(User userAuthor);
    Set<Post> findPostsByMarkListEquals(Set<Mark> markList);
    Set<Post> findPostsByDateCreatedEquals(Date dateCreated);
    Set<Post> findPostsByDateOfEditingEquals(Date dateOfEditing );

}
