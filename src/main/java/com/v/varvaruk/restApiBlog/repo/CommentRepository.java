package com.v.varvaruk.restApiBlog.repo;

import com.v.varvaruk.restApiBlog.entities.Comment;
import com.v.varvaruk.restApiBlog.entities.Post;
import com.v.varvaruk.restApiBlog.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.Set;

public interface CommentRepository extends JpaRepository<Comment,Long> {

    Set<Comment> findByDateCreatedEquals(Date dateCreated );
    Set<Comment> findByDateOfEditingEquals(Date dateOfEditing);
    Set<Comment> findByUserAuthorEquals(User userAuthor);
    Set<Comment> findByPostEquals(Post post);

}
