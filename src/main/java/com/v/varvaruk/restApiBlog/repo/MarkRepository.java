package com.v.varvaruk.restApiBlog.repo;

import com.v.varvaruk.restApiBlog.entities.Mark;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MarkRepository extends JpaRepository<Mark,Long> {
    Optional<Mark> findByNameEquals(String name);



}
