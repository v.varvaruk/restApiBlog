package com.v.varvaruk.restApiBlog.repo;


import com.v.varvaruk.restApiBlog.entities.Role;
import com.v.varvaruk.restApiBlog.entities.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}