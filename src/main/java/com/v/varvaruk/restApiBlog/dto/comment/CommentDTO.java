package com.v.varvaruk.restApiBlog.dto.comment;

import com.v.varvaruk.restApiBlog.dto.user.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentDTO {

    private Long id;
    private String text;
    private Date dateCreated;
    private Date dateOfEditing;
    private UserDTO userAuthor;


}
