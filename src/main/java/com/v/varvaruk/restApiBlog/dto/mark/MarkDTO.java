package com.v.varvaruk.restApiBlog.dto.mark;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarkDTO {

    private Long id;
    @NotBlank
    private String name;



}

