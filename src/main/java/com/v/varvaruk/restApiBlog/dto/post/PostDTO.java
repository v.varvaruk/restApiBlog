package com.v.varvaruk.restApiBlog.dto.post;

import com.v.varvaruk.restApiBlog.dto.user.UserDTO;
import com.v.varvaruk.restApiBlog.dto.comment.CommentDTO;
import com.v.varvaruk.restApiBlog.dto.mark.MarkDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostDTO {
    private Long id;
    @NotBlank
    private String title;
    @NotBlank
    private String body;
    private Date dateCreated;
    private Date dateOfEditing;
    private UserDTO userAuthor;
    private Set<MarkDTO> markList;
    private List<CommentDTO> commentList;
}
