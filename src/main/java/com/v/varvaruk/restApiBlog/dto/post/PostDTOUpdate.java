package com.v.varvaruk.restApiBlog.dto.post;

import com.v.varvaruk.restApiBlog.dto.mark.MarkDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostDTOUpdate {
    private Long id;
    @NotBlank
    private String title;
    @NotBlank
    private String body;
    private Set<MarkDTO> markList;
  }
